FROM python:3.6-jessie

RUN apt-get update -y && \
    apt-get install -y python-pip bash awscli && \
    pip install --upgrade pip

COPY requirements.txt .
RUN pip install -r requirements.txt

