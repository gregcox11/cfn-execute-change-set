from boto3 import client
from retrying import retry
from argparse import ArgumentParser
from sys import exit
from datetime import datetime

REGION = 'us-east-1'

stackNotExistStatus = [
    'DELETE_COMPLETE'
]

def get_content(filepath):
    with open(filepath) as data:
        return data.read()

@retry(wait_exponential_multiplier=500, wait_exponential_max=10000, stop_max_delay=10000)
def get_change_set_ready(stackName):
    cf_conn = client('cloudformation', region_name=REGION)
    print(f"{datetime.now()}::Checking if change set exists")
    try:
        response = cf_conn.list_change_sets(StackName=stackName)
        print(f"{datetime.now()}::Change set does exist:")
        if response.get("Summaries") == []:
            pass
        elif response["Summaries"][0]["StackName"] == stackName and response["Summaries"][0]["Status"] == "FAILED":
            # add a try accept for delete, and raise if fails
            del_response = cf_conn.delete_change_set(
                ChangeSetName=response["Summaries"][0]["ChangeSetId"],
                StackName=stackName
            )
            if del_response["ResponseMetadata"]["HTTPStatusCode"] == 200:
                return True
        else:
            del_response = cf_conn.delete_change_set(
                ChangeSetName=response["Summaries"][0]["ChangeSetId"],
                StackName=stackName
            )
            if del_response["ResponseMetadata"]["HTTPStatusCode"] == 200:
                return True            
        return True

    except cf_conn.exceptions.ClientError as error:
        if "does not exist" in error.response["Error"]["Message"]:
            print(f"{datetime.now()}::Change set does not exist:")
            return True


def get_stack_exist(stackName):
    cf_conn = client('cloudformation', region_name=REGION)

    stacks = cf_conn.list_stacks()
    for stack in stacks['StackSummaries']:
        if stackName == stack['StackName'] and stack['StackStatus'] != stackNotExistStatus[0]:
                print(f"{datetime.now()}::Stack does exist:")
                return True
        else:
            continue
    print(f"{datetime.now()}::Stack does not exist:")
    return False

def create_change_set(stackName, templatePath, changeSetName, parameters, capabilities, changeSetType="CREATE"):
    cf_conn = client('cloudformation', region_name=REGION)
    print(f"{datetime.now()}::Identifying if stack already exists:")

    # Identify if stack already exists
    changeSetType = "UPDATE" if get_stack_exist(stackName) else "CREATE"

    # get changeSets identify status of changeSet, assigning to print later if needed
    if changeSetType == "UPDATE":
        get_change_set_ready(stackName)

    print(f"{datetime.now()}::Creating change set:")
    response = cf_conn.create_change_set(
        StackName=stackName,
        ChangeSetName=changeSetName,
        ChangeSetType=changeSetType,
        TemplateBody=get_content(templatePath),
        Parameters=parameters,
        Capabilities=[capabilities]
    )
    response["operation"] = changeSetType
    print(f"{datetime.now()}::Change set created:")
    return response

# def retry_if_result_404(result):
#     """Return True if we should retry (in this case when result is None), False otherwise"""
#     return result['statusCode'] == 404

@retry(wait_exponential_multiplier=500, wait_exponential_max=10000, stop_max_delay=10000)
def execute_change_set_w_id(changeSetId, stackName, operation):
    cf_conn = client('cloudformation', region_name=REGION)
    # print(f"Executing change set: {changeSetId}")
    try:
        response = cf_conn.execute_change_set(ChangeSetName=changeSetId)
        # print(f"execute response {response}")
    except cf_conn.exceptions.InvalidChangeSetStatusException as error:
        if "FAILED" in error.response["Error"]["Message"]:
            response = cf_conn.describe_change_set(
                ChangeSetName=changeSetId,
                StackName=stackName
            )
            if "The submitted information didn't contain changes" in response["StatusReason"]:
                print(f"{datetime.now()}::No changes in submitted template")
                exit(0)
            else:
                print(f"{datetime.now()}::Execution of change set failed: {error.response['Error']['Message']}")
        else:
            if "CREATE_IN_PROGRESS" in error.response['Error']['Message']:
                print(f"{datetime.now()}::Initiating change set execution: CREATE IN PROGRESS")
                exit(1)    
            elif "CREATE_PENDING" in error.response['Error']['Message']:
                print(f"{datetime.now()}::Initiating change set execution: CREATE PENDING")
                exit(1)
            elif "EXECUTE_IN_PROGRESS" in error.response['Error']['Message']:
                print(f"{datetime.now()}::Initiating change set execution: EXECUTE PENDING")
                exit(1)
            print(f"{datetime.now()}::Execute_error==={error.response['Error']}")
            exit(1)

    if operation == "UPDATE":
        print(f"{datetime.now()}::Execution initiated, waiting on stack update:")
        waiter = cf_conn.get_waiter('stack_update_complete')
    else:
        print(f"{datetime.now()}::Execution initiated, waiting on stack creation:")
        waiter = cf_conn.get_waiter('stack_create_complete')

    waiter.wait(StackName=stackName)
    print(f"{datetime.now()}::Execution completed successfully:")
    return describe_stack_by_name(stackName)

@retry(wait_exponential_multiplier=500, wait_exponential_max=10000, stop_max_delay=10000)
def describe_stack_by_name(stackName):
    cf_conn = client('cloudformation', region_name=REGION)
    return cf_conn.describe_stacks(StackName=stackName)

def parseParams(params):
    parameters = []
    if params:
        params = params[0].split(" ")
        for p in params:
            _, pKey, pValue = p.split("=")
            parameters.append({"ParameterKey": pKey.split(",")[0], "ParameterValue": pValue})
    return parameters

if __name__ == '__main__':

    parser = ArgumentParser()
    parser.add_argument("--stack-name", action="store", dest="stackName", help="Enter stack name")
    parser.add_argument("--region", action="store", dest="region", default="us-east-1", help="AWS Region")
    parser.add_argument("--change-set-name", action="store", dest="changeSet", help="Change Set Name")
    parser.add_argument("--template-path", action="store", dest="templatePath", help="Enter local path to template file")
    parser.add_argument("--parameters", action="store", dest="params", nargs='+', help="Enter one or separated by spaces 'ParameterKey=<key>,ParameterValue=<value>'")
    parser.add_argument("--capabilities", action="store", dest="capabilities", default="CAPABILITY_NAMED_IAM", help="Enter one of 'CAPABILITY_IAM'|'CAPABILITY_NAMED_IAM'|'CAPABILITY_AUTO_EXPAND'")
    # parser.add_argument("--", action="store", dest="", help="")
    args = parser.parse_args()
    create_resp = create_change_set(args.stackName, args.templatePath, args.changeSet, parseParams(args.params), args.capabilities)
    cf_conn = client('cloudformation', region_name=REGION)
    response = cf_conn.describe_change_set(
        ChangeSetName=create_resp["Id"],
        StackName=args.stackName
    )
    execute_resp = execute_change_set_w_id(create_resp["Id"], args.stackName, create_resp["operation"])
    response = cf_conn.describe_change_set(
        ChangeSetName=create_resp["Id"],
        StackName=args.stackName
    )
    print(f"{datetime.now()}::Changes==={response['Changes']}")


# python3 cfn-execute-change-set.py --stack-name="WPtest4" --region="us-east-1" --change-set-name="WPtest-ChangeSet" --template-path="/Users/gregory.cox/Documents/projects/cto-afactor-api/cloudformation/samples/WordPress_Multi_AZ.json" --capabilities="CAPABILITY_NAMED_IAM" --parameters="ParameterKey=VpcId,ParameterValue=vpc-dcf944b8 ParameterKey=Subnets,ParameterValue=subnet-6713e611,subnet-6fcfc036,subnet-7919841c ParameterKey=KeyName,ParameterValue=cto_afactor_api ParameterKey=DBUser,ParameterValue=dbadmin ParameterKey=DBPassword,ParameterValue=test12345"

# stackExistStatus = [
#     'CREATE_IN_PROGRESS',
#     'CREATE_FAILED',
#     'CREATE_COMPLETE',
#     'ROLLBACK_IN_PROGRESS',
#     'ROLLBACK_FAILED',
#     'ROLLBACK_COMPLETE',
#     'DELETE_IN_PROGRESS',
#     'DELETE_FAILED',
#     'UPDATE_IN_PROGRESS',
#     'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS',
#     'UPDATE_COMPLETE',
#     'UPDATE_ROLLBACK_IN_PROGRESS',
#     'UPDATE_ROLLBACK_FAILED',
#     'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS',
#     'UPDATE_ROLLBACK_COMPLETE',
#     'REVIEW_IN_PROGRESS'
# ]
